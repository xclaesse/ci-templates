# Makefile for convenience and easier discoverability

all: templates

# Let's not bother with proper tracking of template files, we have the CPU
# bandwith to generate a few files
.PHONY: templates doc
templates:
	./src/generate_templates.py

doc:
	./doc/build-docs.sh

# Really, this job only exists to show that ci-fairy (and setup.py)
# is not part of the normal templates generation process.
ci_fairy_install:
	pip install .

